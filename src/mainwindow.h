/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Alexander Bokovoy <alexander.bokovoy@nokia.com>
**
** This file is part of the Quill Demo package.
**
** Commercial Usage
** Licensees holding valid Qt Commercial licenses may use this file in
** accordance with the Qt Commercial License Agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Nokia.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain
** additional rights. These rights are described in the Nokia Qt LGPL
** Exception version 1.0, included in the file LGPL_EXCEPTION.txt in this
** package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <Qt>
#include <QTime>
#include <QTimer>
#include "ui_menu.h"

class QAction;
class QToolBar;
class QMenu;
class Quill;
class ImageScene;
class RectImage;
class QString;
class ViewScrollBar;
class QCloseEvent;
class QSize;
class QFont;
class QColor;
class QPolygon;
class QuillFile;
class ColorsDialog;
class MainWindow : public QMainWindow, public Ui_ImageEditor
 {
     Q_OBJECT

 public:
     MainWindow();
     ~MainWindow();
     void getFileName(const QString flieName);
     void storeCacheLimit(const QString resolution, const QString cost);
     void recover();
     void load(const QString &dirName = "");

 private:
     qreal min(qreal number1, qreal number2);
     void scaleGraphicsView();
     //for grid view to editing mode
     void loadBigImageFile(QString fileName);

 private slots:
     void changeMode();

     void save();
     void updateZoomLabel();
     void zoomIn();
     void zoomOut();
     void refresh();
     void refreshPartial();
     void increaseBrightness();
     void increaseContrast();
     void decreaseContrast();
     void scale();
     void rotateFree();
     void undo();
     void redo();
     void decreaseBrightness();
     void clockwiseRotate();
     void counterClockwiseRotate();
     void flipVertical();
     void flipHorizontal();
     QRect viewPort();
     void fastCrop();
     void fastRotate();

     void autoContrast();
     void autoLevels();
     void redEyeReduction(QPointF point);

     void focusChanged();
     QPointF transformSceneToFullImage(QPointF scene);

     //grid view to editing mode
     void refreshSingleImageView();
     void loadBigImage(int ithImage);
     void returnGridView();
     //crop
     void drawCropFloatingArea(QPointF point);
     void changingCropFloatingArea(QPointF point2);
     void crop(QPointF center);
     void addCropOk();

     void revert();
     void restore();

     void displayMetadata();

     void closeEvent(QCloseEvent *event);
 private:

     ImageScene *imageScene, *singleImageScene;
     RectImage *rectImage, *rectPartialImage;
     QString fileName;
     QString editHistoryFileName;
     QScrollBar *hScrollBar;
     QScrollBar *vScrollBar;
     //we need some timer to test
     QTime timeLoad;
     QTime timeZoom;
     QTime timeRotate;
     QTime timeDraw;
     qreal zoomLevel;
     int debugDelay;

     int hValue;
     int vValue;

     bool singleImageMode;

     int hBigValue;
     int vBigValue;
     int hBigMaxiValue;
     int vBigMaxiValue;
     int hSmallValue;
     int vSmallValue;
     int hSmallMaxiValue;
     int vSmallMaxiValue;
     QSize imageSize;
     qreal screenRatio;
     qreal imageRatio;

     //text and balloon
     QFont font;
     QColor color;
     QString inputText;
     QPointF center;

     QuillFile *quillFile;

     //grid view
     QList<QGraphicsItem*> rectImageList;
     QList<QuillFile*> quillFileList;
     RectImage *rectBigImage;

     // canvas area for tiles to be drawn on
     QRect prevBounding;
     QImage tileImage;

     //crop floating selection area
     double cropAreaWidth;
     double cropAreaHeight;
     QGraphicsRectItem *cropArea;
     QGraphicsLineItem* grabLine;
     QGraphicsRectItem *bgCropArea;
     QGraphicsLineItem* bgGrabLine;
     QPointF cropRectBottomRight;
     QPointF cropRectCenter;
     QGraphicsTextItem* textOk;
     QPointF previousPoint;

     ColorsDialog *colorsDialog;
     QTimer timer;
};

#endif
