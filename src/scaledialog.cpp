/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Alexander Bokovoy <alexander.bokovoy@nokia.com>
**
** This file is part of the Quill Demo package.
**
** Commercial Usage
** Licensees holding valid Qt Commercial licenses may use this file in
** accordance with the Qt Commercial License Agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Nokia.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain
** additional rights. These rights are described in the Nokia Qt LGPL
** Exception version 1.0, included in the file LGPL_EXCEPTION.txt in this
** package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
****************************************************************************/

#include <Qt>
#include "scaledialog.h"
#include <QDebug>
ScaleDialog::ScaleDialog():aspectRatio(0.0)
{
    setupUi(this);

    //we add connection here
    widthSlider->setMinimum(0);
    widthSlider->setMaximum(4000);
    heightSlider->setMinimum(0);
    heightSlider->setMaximum(4000);

    connect(widthSlider, SIGNAL(valueChanged(int)),
            this, SLOT(widthChanged(int)));

    connect(heightSlider, SIGNAL(valueChanged(int)),
            this, SLOT(heightChanged(int)));

    connect(widthMinusButton, SIGNAL(clicked()),
            this, SLOT(widthMinus()));

    connect(widthPlusButton, SIGNAL(clicked()),
            this, SLOT(widthPlus()));

    connect(heightMinusButton, SIGNAL(clicked()),
            this, SLOT(heightMinus()));

    connect(heightPlusButton, SIGNAL(clicked()),
            this, SLOT(heightPlus()));

    connect(checkBox, SIGNAL(stateChanged(int)),this, SLOT(makeConnection(int)));
}

void ScaleDialog::makeConnection(int state)
{
    if(state == Qt::Checked){
        connect(widthSlider,SIGNAL(valueChanged(int)),this, SLOT(changeSlider2Value(int)));
        connect(heightSlider,SIGNAL(valueChanged(int)),this, SLOT(changeSlider1Value(int)));
    }
    else {
        disconnect(widthSlider,SIGNAL(valueChanged(int)),this, SLOT(changeSlider2Value(int)));
        disconnect(heightSlider, SIGNAL(valueChanged(int)),this, SLOT(changeSlider1Value(int)));
    }
}

void ScaleDialog::changeSlider1Value(int value)
{
    int trueValue = value/aspectRatio;
    widthSlider->setValue(trueValue);
}

void ScaleDialog::changeSlider2Value(int value)
{
    int trueValue = aspectRatio*value;
    if (abs(trueValue-heightSlider->value()) > 1)
        heightSlider->setValue(trueValue);
}

void ScaleDialog::setMax(QSize size)
{
    widthSlider->setValue(size.width());
    heightSlider->setValue(size.height());
    widthSlider->setMaximum(size.width());
    heightSlider->setMaximum(size.height());
    aspectRatio = size.height() * 1.0 / size.width();
}

void ScaleDialog::widthChanged(int value)
{
    widthSlider->setValue(value);
    widthLabel->setText(QString::number(value));
}

void ScaleDialog::heightChanged(int value)
{
    heightSlider->setValue(value);
    heightLabel->setText(QString::number(value));
}

void ScaleDialog::widthMinus()
{
    widthSlider->setValue(widthSlider->value() - 1);
}

void ScaleDialog::widthPlus()
{
    widthSlider->setValue(widthSlider->value() + 1);
}

void ScaleDialog::heightMinus()
{
    heightSlider->setValue(heightSlider->value() - 1);
}

void ScaleDialog::heightPlus()
{
    heightSlider->setValue(heightSlider->value() + 1);
}


ScaleDialog::~ScaleDialog()
{
}
