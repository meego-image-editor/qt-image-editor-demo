/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Alexander Bokovoy <alexander.bokovoy@nokia.com>
**
** This file is part of the Quill Demo package.
**
** Commercial Usage
** Licensees holding valid Qt Commercial licenses may use this file in
** accordance with the Qt Commercial License Agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Nokia.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain
** additional rights. These rights are described in the Nokia Qt LGPL
** Exception version 1.0, included in the file LGPL_EXCEPTION.txt in this
** package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
****************************************************************************/

#include <unistd.h>
#include <QApplication>
#include <QDebug>
#include "mainwindow.h"

int c;
char *fileName;
char *resolution;
char *limit;
extern char *optarg;
extern int optind;
extern int optopt;
extern int opterr;

int main(int argc, char *argv[])
{
    bool rootEnabled = false;

    QApplication app(argc, argv);
    MainWindow mainWindow;
    while((c = getopt(argc, argv, "f:s:b:p:ruh")) != -1){
        switch(c){
        case 'f':
            mainWindow.getFileName(optarg);
            break;
        case 's':
            mainWindow.storeCacheLimit("0", optarg);
            break;
        case 'b':
            mainWindow.storeCacheLimit("1", optarg);
            break;
        case 'r':
            mainWindow.recover();
            break;
        case 'u':
            rootEnabled = true;
            break;
        case 'h':
            qDebug()<<"Unknown argument"<<optopt;
        case '?':
            qDebug()<<"Please input";
            qDebug()<<"-f filename";
            qDebug()<<"-r to recover crashed session";
            qDebug()<<"-s small pic cache limit (bytes)";
            qDebug()<<"-b big pic memory (bytes)";
            qDebug()<<"-u enable root";
            break;
        case ':':
            qDebug()<<optopt<<"without argument";
            break;
        }
    }
    if (!rootEnabled) {
        if (getuid() == 0) {
            qDebug("QuillDemo: Running as root not allowed, application closing.");
            qDebug("Run with -u to override (some features might not be available).");
            exit(1);
        }
    }

    mainWindow.load();
    mainWindow.show();
    return app.exec();
}
