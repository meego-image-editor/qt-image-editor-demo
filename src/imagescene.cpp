/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Alexander Bokovoy <alexander.bokovoy@nokia.com>
**
** This file is part of the Quill Demo package.
**
** Commercial Usage
** Licensees holding valid Qt Commercial licenses may use this file in
** accordance with the Qt Commercial License Agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Nokia.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain
** additional rights. These rights are described in the Nokia Qt LGPL
** Exception version 1.0, included in the file LGPL_EXCEPTION.txt in this
** package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
****************************************************************************/

#include <QGraphicsLineItem>
#include <QPoint>
#include <QPen>
#include <QuillImageFilter>
#include <QVariant>
#include <QPainter>
#include <QGraphicsItem>
#include <QStyleOptionGraphicsItem>
#include <QDebug>
#include <cmath>
#include "imagescene.h"

ImageScene::ImageScene():drawlineFlag(false),firstClick(true)
{
    execution = movement;
    drawingStatus = false;
    connect(this,SIGNAL( mousePressed3()),this,SLOT(ableDrawingStatus()));
    connect(this,SIGNAL( mouseReleased1()),this,SLOT(disableDrawingStatus()));
}


void ImageScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    emit focusChanged();
}

void ImageScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    switch(execution){
    case crop:
        //we try to draw the floating cropping area
        //cropStartPosition = mouseEvent->scenePos();
        //execution = cropInProgress;
        //cropCenterPosition = mouseEvent->scenePos();
        execution = cropArea;
        emit drawCropArea(mouseEvent->scenePos());
        break;
    case cropArea:
        emit drawCropArea(mouseEvent->scenePos());
        break;
    case changeCropArea:
        emit changingCropArea(mouseEvent->scenePos());
        break;
    case changeCropAreaDone:
        execution = waiting;
        break;
    case insertText:
        emit mousePressed(mouseEvent->scenePos());
        break;
    case addTextBalloon:
        if(firstClick){
            emit mousePressed2(mouseEvent->scenePos());
            firstClick = false;
        }
        else{
            emit mousePressed1(mouseEvent->scenePos());
            firstClick = true;
        }
        break;
    case brushDraw:
	emit mousePressed3();
        break;
    case redEyeReduction:
        emit redEyeReduce(mouseEvent->scenePos());
        break;
    case gridView:
        emit editingModeStart(floor(mouseEvent->scenePos().y()/128)*4+floor(mouseEvent->scenePos().x()/128));
        break;
    default:
        break;
    }
}

void ImageScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    switch(execution){
    case brushDraw:
        emit mouseReleased1();
        break;
    case  cropInProgress:
        emit cropDone(cropStartPosition, QPointF(mouseEvent->scenePos()));
        execution = crop;
        break;
    default:
        break;
    }
}

ImageScene::~ImageScene()
{
}

void ImageScene::setMode(Mode mode)
{
    execution = mode;
}

ImageScene::Mode ImageScene::mode()
{
    return execution;
}


void ImageScene::setFirstClick()
{
    firstClick = true;
}

void ImageScene::ableDrawingStatus()
{
    drawingStatus = true;
}

void ImageScene::disableDrawingStatus()
{
    drawingStatus = false;
}
